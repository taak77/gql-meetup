# GraphQL Server with MongoDB

### Creating MongoDB Sharding Cluster

Create MongoDB with Sharding and Replica set
```
$ mongod --shardsvr --port 27010 --dbpath /mongodb/db0 --replSet rs0
$ mongod --shardsvr --port 27011 --dbpath /mongodb/db1 --replSet rs0
$ mongod --shardsvr --port 27012 --dbpath /mongodb/db2 --replSet rs0
$ mongod --shardsvr --port 27013 --dbpath /mongodb/db3 --replSet rs1
$ mongod --shardsvr --port 27014 --dbpath /mongodb/db4 --replSet rs1
$ mongod --shardsvr --port 27015 --dbpath /mongodb/db5 --replSet rs1

$ mongo --port=27010
$ rs.initiate()
$ rs.add('localhost:27011') # or pass config options in `rs.initiate()`
$ rs.add('localhost:27012')

$ mongo --port=27013
$ rs.initiate()
$ rs.add('localhost:27014') # or pass config options in `rs.initiate()`
$ rs.add('localhost:27015')
```

Create Config Server
```
$ mongod --configsvr --port 27016 --dbpath /mongodb/configdb0 --replSet configReplSet
$ mongod --configsvr --port 27017 --dbpath /mongodb/configdb1 --replSet configReplSet
$ mongod --configsvr --port 27018 --dbpath /mongodb/configdb2 --replSet configReplSet

$ mongo --port=27016
$ rs.initiate()
$ rs.add('localhost:27017') # or pass config options in `rs.initiate()`
$ rs.add('localhost:27018')
```

Create Router
```
$ mongos --configdb configReplSet/localhost:27016,localhost:27017,localhost:27018 --port 27019

$ mongo --port 27019
$ sh.addShard("rs0/localhost:27010,localhost:27011,localhost:27012")
$ sh.addShard("rs1/localhost:27013,localhost:27014,localhost:27015")
```

Enable Sharding for `authors` and `books` collections
```
$ sh.enableSharding('gql-meetup')
$ db.authors.createIndex({authorId: 1}) #when the collection is not empty
$ sh.shardCollection('gql-meetup.authors', {authorId: 'hashed'})
$ db.books.createIndex({_id: 1}) #when the collection is not empty
$ sh.shardCollection('gql-meetup.books', {_id: 'hashed'})
```
