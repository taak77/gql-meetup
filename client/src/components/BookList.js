import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { getBooksQuery } from '../queries';


class BookList extends Component {
    displayBooks() {
        const {data} = this.props;
        console.log('data', data);
        if (data.error) {
            return (<div style={{color: 'red'}}>{data.error.message}</div>)
        } else if (data.loading) {
            return (<div>Loading...</div>)
        } else {
            if (!data.books.length) {
                return (<div>Book List is empty</div>)
            }
            return data.books.map(book => (
                <li key={book.id}>{book.name}</li>
            ));
        }
    }

    render() {
        return (
            <div>
                <ul id="book-list">
                    {this.displayBooks()}
                </ul>
            </div>
        );
    }
}

export default graphql(getBooksQuery)(BookList);
