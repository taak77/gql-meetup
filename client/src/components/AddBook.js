import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';
import { getAuthorsQuery, addBookMutation, getBooksQuery } from '../queries';

class AddBook extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            genre: '',
            authorId: ''
        };
    }

    onChange = event => {
        const {target} = event;
        const {id, value} = target;
        this.setState({[id]: value});
    }

    displayAuthors() {
        const {data} = this.props;
        if (data.error) {
            return (<div>{data.error.message}</div>)
        } else if (data.loading) {
            return (<option>Loading...</option>);
        } else {
            return data.authors.map(author => {
                return (<option key={author.authorId} value={author.authorId}>{author.name}</option>);
            });
        }
    }

    submitForm = (event) => {
        event.preventDefault();
        const {name, genre, authorId} = this.state;
        console.log(name, genre, authorId);
        this.props.addBookMutation({
            variables: {
                name,
                genre,
                authorId
            },
            refetchQueries: [{query: getBooksQuery}]
        });
    }

    render() {
        return (
            <form id="add-book" onSubmit={this.submitForm}>
                <div className="field">
                    <label>Book Name:</label>
                    <input id="name" type="text" onChange={this.onChange}/>
                </div>

                <div className="fields">
                    <label>Genre:</label>
                    <input id="genre" type="text" onChange={this.onChange}/>
                </div>

                <div className="field">
                    <label>Author:</label>
                    <select id="authorId" onChange={this.onChange}>
                        <option>Select author</option>
                        {this.displayAuthors()}
                    </select>
                </div>
                <button>+</button>
            </form>
        );
    }
}

export default compose(
    graphql(getAuthorsQuery),
    graphql(addBookMutation, {name: "addBookMutation"})
)(AddBook);
