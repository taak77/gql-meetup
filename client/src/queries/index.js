import {gql} from 'apollo-boost';

export const getAuthorsQuery = gql`
	{
		authors {
			id
			name
			authorId
		}
	}
`;

export const getBooksQuery = gql`
	{
		books {
			id
			name
			author {
			    name
			    authorId
			}
		}
	}
`;

export const addBookMutation = gql`
	mutation ($name: String!, $genre: String!, $authorId:String!) {
		addBook(name: $name, genre: $genre, authorId: $authorId) {
			id
			name
			author {
			    name
			    authorId
			}
		}
	}
`;
