const fs = require('fs');
const {promisify} = require('util');
const mongoose = require('mongoose');
const argv = require('yargs').argv;
const Book = require('./models/book');
const Author = require('./models/author');

const readFileAsync = promisify(fs.readFile);

function readAndInsert(file, model) {
    return readFileAsync(file)
        .then(content => {
            const data = JSON.parse(content);
            return model.insertMany(data);
        });
}

const {host = 'localhost', port = '27017', db = 'gql-meetup'} = argv;
mongoose.connect(`mongodb://${host}:${port}/${db}`, {useNewUrlParser: true});
mongoose.connection.once('open', async () => {
    console.log('connected to database');

    try {
        await Promise.all([
            readAndInsert('./data/authors.json', Author),
            readAndInsert('./data/books.json', Book)
        ]);
        console.log('successfully wrote to database');
        process.exit(0);
    } catch (err) {
        console.log(err);
        process.exit(1);
    }
});
