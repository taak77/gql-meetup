var fs = require('fs');
var readline = require('readline');
var stream = require('stream');

var inputfile = "./authors.txt";
inputfile = '/Users/taak77/Downloads/ol_dump_authors_2018-08-31.txt';
var outputfile = "authors.json";

var instream = fs.createReadStream(inputfile);
var outstream = fs.createWriteStream(outputfile, {'flags': 'a'});
outstream.readable = true;
outstream.writable = true;
outstream.write('[' + '\n');
var rl = readline.createInterface(instream, outstream);

rl.on("line", function(line){this.emit("pause", line);});

let isFirstLine = true;
rl.on("pause", function(line) {

    //console.log("pause");
    //console.log("doing some work");

    // do some work here
    let data;
    const result = /[^\{]+(\{.+\})$/.exec(line);
    if (result && result[1]) {
        try {
            const { name, key } = JSON.parse(result[1]);
            data = {
                name,
                authorId: key.split('/').pop()
            }
        } catch (err) {
            console.log(err);
        }
    }
    if (data) {
        //console.log(data);
        outstream.write(`${isFirstLine ? '' : ',\n'}${JSON.stringify(data)}`);
    }
    isFirstLine = false;
    this.emit("resume");

});

rl.on("resume", function() {
    //console.log("resume");
});

rl.on("close", function() {
    console.log("close");
    outstream.write('\n' + ']');
});
