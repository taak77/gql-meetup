const express = require('express');
const graphqlHTTP = require('express-graphql');
const schema = require('./schema');
const mongoose = require('mongoose');
const cors = require('cors');
const config = require('getconfig');
const argv = require('yargs').argv;

const app = express();
app.use(cors());

const {port, db: {url: dbUrl}} = config;
console.log('dbUrl', dbUrl)
mongoose.connect(dbUrl, {
    useNewUrlParser: true,
    reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
    reconnectInterval: 500, // Reconnect every 500ms
    poolSize: 10, // Maintain up to 10 socket connections
    // If not connected, return errors immediately rather than waiting for reconnect
    bufferMaxEntries: 0,
    connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
    socketTimeoutMS: 45000
});
mongoose.connection.once('open', () => {
	console.log('connected to database');
});
mongoose.connection.on('error', console.error);

// middleware
app.use('/graphql', graphqlHTTP({
	schema,
	graphiql: true
}));

// TODO: fix this hardcoded port
app.listen(port, () => {
	console.log(`Now listening for requests on port ${port}`);
});
