var fs = require('fs');

// retrieved from
// https://openlibrary.org/subjects/fantasy.json?limit=100&has_fulltext=false&sort=editions
// https://openlibrary.org/subjects/sci-fi.json?limit=100&has_fulltext=false&sort=editions
const files = ['./data/fantasy.json', './data/sci-fi.json'];

const books = [];
const authorsData = [];

files.forEach(file => {
    const data = fs.readFileSync(file);
    const {name, works} = JSON.parse(data);
    const genre = `${name.charAt(0).toUpperCase()}${name.substr(1)}`;
    works.forEach(({title, authors = []}) => {
        const author = authors[0];
        if (author) {
            const authorId = author.key.split('/').pop();
            if (!books.find(item => item.name === title)) {
                books.push({name: title, genre, authorId});
            }
            if (!authorsData.find(item => item.authorId === authorId)) {
                authorsData.push({name: author.name, authorId});
            }
        }
    });
});

try {
    fs.unlinkSync('./data/authors.json');
    fs.unlinkSync('./data/books.json');
    console.log('successfully deleted authors.json');
} catch (err) {
    console.log(err);
}

try {
    fs.writeFileSync('./data/authors.json', JSON.stringify(authorsData));
    fs.writeFileSync('./data/books.json', JSON.stringify(books));
    console.log('successfully wrote authors.json');
} catch (err) {
    console.log(err);
}

