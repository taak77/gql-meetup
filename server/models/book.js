const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bookSchema = new Schema({
    id: Schema.Types.ObjectId,
	name: String,
	genre: String,
	authorId: String
}, {
    shardKey: { _id: 'hashed' },
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

module.exports = mongoose.model('Book', bookSchema, 'books');
