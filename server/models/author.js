const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const authorSchema = new Schema({
    id: Schema.Types.ObjectId,
	name: String,
	authorId: String,
}, {
    shardKey: { authorId: 'hashed' },
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

module.exports = mongoose.model('Author', authorSchema, 'authors');
