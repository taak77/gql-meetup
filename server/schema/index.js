const graphql = require('graphql');
const mongoose = require('mongoose');
const Book = require('../models/book');
const Author = require('../models/author');
const {
	GraphQLObjectType,
	GraphQLInputObjectType,
	GraphQLString,
	GraphQLSchema,
	GraphQLID,
	GraphQLList,
	GraphQLNonNull
} = graphql;

const books = [
	{name: 'Name of the Wind', genre: 'Fantasy', id: '1', authorId: '1'},
	{name: 'The Final Empire', genre: 'Fantasy', id: '2', authorId: '2'},
	{name: 'The Long Earth', genre: 'Sci-Fi', id: '3', authorId: '3'},
	{name: 'The Hero of Ages', genre: 'Fantasy', id: '4', authorId: '2'},
	{name: 'The Colour of Magic', genre: 'Fantasy', id: '5', authorId: '3'},
	{name: 'The Light Fantastic', genre: 'Fantasy', id: '6', authorId: '3'},
];

const authors = [
	{name: 'Patrick Rothfuss', age: 44, id: '1'},
	{name: 'Brandon Sanderson', age: 42, id: '2'},
	{name: 'Terry Pratchett', age: 66, id: '3'}
];

// defines schema, relationships, and root queries

const BookType = new GraphQLObjectType({
	name: 'Book',
	fields: () => ({
		id: {
            type: GraphQLID,
            resolve(parent) {
                return parent._id.toString();
            }
        },
		name: {type: GraphQLString},
		genre: {type: GraphQLString},
		// 5n
		author: {
			type: AuthorType,
			// parent, args
			/* async resolve(parent) {
                try {
                    const doc = await Author.find({authorId: parent.authorId});
                    return doc && doc[0];
                } catch (err) {
                    return err;
                }
			} */
		}
	})
});

const AuthorType = new GraphQLObjectType({
	name: 'Author',
	// why is this wrapped in a function?
	// JS does a two passes
	fields: () => ({
		id: {
            type: GraphQLID,
            resolve(parent) {
                return parent._id.toString();
            }
        },
		name: {type: GraphQLString},
        authorId: {type: GraphQLString},
		books: {
			type: new GraphQLList(BookType),
			/* resolve(parent) {
				// return _.filter(books, {authorId: parent.id});
				return Book.find({authorId: parent.authorId});
			} */
		},
	})
});

const RootQuery = new GraphQLObjectType({
	name: 'RootQueryType',
	fields: {
		book: {
			type: BookType,
			args: {id: {type: GraphQLID}},
			resolve(parent, args) {
                return Book.aggregate([
                    {
                        $match: {_id: mongoose.Types.ObjectId(args.id)}
                    },
                    {
                        $lookup: {
                            from: 'authors',
                            localField: 'authorId',
                            foreignField: 'authorId',
                            as: 'author'
                        }
                    },
                    {
                        $project: {
                            name: 1,
                            genre: 1,
                            author: {
                                $arrayElemAt: [ '$author', 0 ]
                            }
                        }
                    }
                ])
                .then(docs => {
                    //console.log('docs', docs)
                    return docs && docs[0];
                });
				// return Book.findById(args.id);
			}
		},
		author: {
			type: AuthorType,
			args: {id: {type: GraphQLID}},
			resolve(parent, args) {
                return Author.aggregate([
                    {
                        $match: {_id: mongoose.Types.ObjectId(args.id)}
                    },
                    {
                        $lookup: {
                            from: 'books',
                            localField: 'authorId',
                            foreignField: 'authorId',
                            as: 'books'
                        }
                    }
                ])
                .then(docs => {
                    //console.log('docs', docs)
                    return docs && docs[0];
                });
				//return Author.findById(args.id);
			}
		},
		books: {
			type: new GraphQLList(BookType),
			// parent, args
			resolve() {
				// return books;
                return Book.aggregate([
                    {
                        $lookup: {
                            from: 'authors',
                            localField: 'authorId',
                            foreignField: 'authorId',
                            as: 'author'
                        }
                    },
                    {
                        $project: {
                            name: 1,
                            genre: 1,
                            author: {
                                $arrayElemAt: [ '$author', 0 ]
                            }
                        }
                    }
                ])
                .then(docs => {
                    //console.log('docs', docs)
                    return docs;
                });
			}
		},
		authors: {
			type: new GraphQLList(AuthorType),
			resolve() {
                // return authors;
                return Author.aggregate([
                    {
                        $lookup: {
                            from: 'books',
                            localField: 'authorId',
                            foreignField: 'authorId',
                            as: 'books'
                        }
                    }
                ]);
				//return Author.find({});
			}
		}
	},
});

const AuthorInputType = new GraphQLInputObjectType({
	name: 'AuthorInputType',
	fields: () => ({
		name: {type: new GraphQLNonNull(GraphQLString)},
        authorId: {type: new GraphQLNonNull(GraphQLString)}
	})
});

const BookInputType = new GraphQLInputObjectType({
	name: 'BookInputType',
	fields: () => ({
		name: {type: new GraphQLNonNull(GraphQLString)},
		genre: {type: new GraphQLNonNull(GraphQLString)},
		authorId: {type: new GraphQLNonNull(GraphQLString)}
	})
});

const Mutation = new GraphQLObjectType({
	name: 'Mutation',
	fields: {
		addAuthor: {
			type: AuthorType,
			args: {
				name: {type: new GraphQLNonNull(GraphQLString)},
				authorId: {type: new GraphQLNonNull(GraphQLString)},
			},
			async resolve(parent, {name, authorId}) {
				try {
					const author = new Author({
						name,
                        authorId
					});
					return author.save();
				} catch (err) {
					return err;
				}
			}
		},
		addBook: {
			type: BookType,
			args: {
				name: {type: new GraphQLNonNull(GraphQLString)},
				genre: {type: new GraphQLNonNull(GraphQLString)},
				authorId: {type: new GraphQLNonNull(GraphQLString)},
			},
			async resolve(parent, {name, genre, authorId}) {
				try {
					const book = new Book({
						name,
						genre,
						authorId
					});
					return book.save();
				} catch (err) {
					return err;
				}
			}
		},
		addAuthors: {
			type: new GraphQLList(AuthorType),
			args: {
				authors: {type: new GraphQLList(AuthorInputType)}
			},
			async resolve(parent, {authors}) {
				try {
					const docs = Author.insertMany(authors);
					return docs;
				} catch (err) {
					return err;
				}
			}
		},
		addBooks: {
			type: new GraphQLList(BookType),
			args: {
				books: {type: new GraphQLList(BookInputType)}
			},
			async resolve(parent, {books}) {
				try {
					const docs = Book.insertMany(books);
					return docs;
				} catch (err) {
					return err;
				}
			}
		}
	}
});

module.exports = new GraphQLSchema({
	query: RootQuery,
	mutation: Mutation
});
