#!/bin/bash

VERSION=1.1.2
SCRIPTNAME=$(basename "$0")
MONGOHOME=/usr/local/Cellar/mongodb/3.6.4
MONGOBIN=$MONGOHOME/bin
MONGOD=$MONGOBIN/mongod

# if [ $# != 1 ]
# then
# 	echo "Usage: $SCRIPTNAME [start|stop|restart]"
# 	exit
# fi

pid() {
    ps -ef | awk '/[m]ongodb/ {print $2}'
}

stopServer() {
    PID=$(pid)
    if [ ! -z "$PID" ];
    then
        echo "... stopping mongodb-server with pid: $PID"
	sudo kill $PID
    else
        echo "... mongodb-server is not running!"
    fi
}

startServer() {
    PID=$(pid)
    if [ ! -z "$PID" ];
    then
        echo "... mongodb-server already running with pid: $PID"
    else
        echo "... starting mongodb-server"
        sudo "$MONGOD" --config "$1"
    fi
}

restartServer() {
    stopServer
    sleep 1s
    startServer $1
}

case "$1" in
	start) startServer $2
	       ;;

	stop) stopServer
	      ;;

	restart) restartServer $2
		 ;;

	*) echo "unknown command"
	   exit
	   ;;
esac

# mongod --shardsvr --port 27010 --dbpath /mongodb/db0 --replSet rs0
# mongod --shardsvr --port 27011 --dbpath /mongodb/db1 --replSet rs0
# mongod --shardsvr --port 27012 --dbpath /mongodb/db2 --replSet rs0
# mongod --shardsvr --port 27013 --dbpath /mongodb/db3 --replSet rs1
# mongod --shardsvr --port 27014 --dbpath /mongodb/db4 --replSet rs1
# mongod --shardsvr --port 27015 --dbpath /mongodb/db5 --replSet rs1
